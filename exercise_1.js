// Exercise 1:
//
// Design a function where you are given the total amount of change wanted
// and an array of coin denominations. Return the number of ways you can combine
// these coin denominations to total the change wanted.

// Sample #01:
//   changeWanted: 4, coins: [1,2,3]
//   Result: 4 === findWaysForChangeGivenCoins(4, [1,2,3])
//     because there are 4 ways to get change for 4 cents:
//   [1,1,1,1], [1,1,2], [2,2], and [1,3]

// Sample #02
//   changeWanted: 10, coins: [2 5 3 6]
//   Result: 5 === findWaysForChangeGivenCoins(10, [2,5,3,6])
//     because there are 5 ways to get 10 cents:
//   [2,2,2,2,2], [2,3,2,3], [2,3,5], [5,5], and [6,2,2]

// HINTS:
// Think about the degenerate cases:
// How many ways can you give change for 0 cents?
// How many ways can you give change for >0 cents, if you have no coins?

// If you are having trouble defining your solutions store, then think about it
// in terms of the base case

// This is a "dynamic programming" problem. There is a brute-force solution, but
// it will likely take exponential time. Dynamic programming problems seem like
// they involve recursion, but they usually just require "storing" the answers to
// the smaller sub-problems.


export default function findWaysForChangeGivenCoins (changeWanted, coins){
  // YOUR CODE HERE
}
