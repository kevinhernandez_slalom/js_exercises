# HOW TO START THESE EXERCISES #

First, install the Node dependencies:

```
npm install
```

and then have the test-watching script run as you edit your code: 
 
```
npm test -s
```

( `-s` means *silent* )

Now you can begin editing the `exercise_0.js` and `exercise_1.js` files until all the unit tests pass. Good luck!