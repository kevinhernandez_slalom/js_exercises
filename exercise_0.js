// Exercise 0:

// Given an input N, return a string that draws a pyramid with height N.

// Input: 4
// Output:
//    #
//   ##
//  ###
// ####

// Input: 6
// Output:
//      #
//     ##
//    ###
//   ####
//  #####
// ######
//

// TIP: Unless the string is empty, *every* line should end in a newline '\n'

export default function drawPyramid(height){
  // YOUR CODE HERE
}
