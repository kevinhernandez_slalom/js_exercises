import chai from 'chai';
const expect = chai.expect;
import drawPyramid from '../exercise_0';

describe('Exercise 0 - Pyramids -', ()=> {

  it('Pyramids of height <1 should return empty string', ()=>{
    expect( drawPyramid(-123) ).to.equal("");
  })

  it('Pyramids of height 1', ()=>{
    expect( drawPyramid(1) ).to.equal("#\n");
  })

  it('Pyramids of height 2', ()=>{
    expect( drawPyramid(2).trim() ).to.equal(" #\n##\n".trim());
  })

  it('Pyramids of height 5', ()=>{
    expect( drawPyramid(5) ).to.equal("    #\n   ##\n  ###\n ####\n#####\n");
  })
})
