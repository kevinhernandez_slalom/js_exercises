import chai from 'chai';

const expect = chai.expect;

import findWaysForChangeGivenCoins from '../exercise_1';

describe('Exercise 1 - Coin Change -', ()=> {

  it('How many ways can you give change for 0 cents, given NO coins?', ()=>{
    expect( findWaysForChangeGivenCoins(0,[]) ).to.equal(1);
  })

  it('... given SOME coins?', ()=>{
    expect( findWaysForChangeGivenCoins(0,[1,2,3]) ).to.equal(1);
  })

  it('How many ways can you give change for >0 cents, given NO coins?', ()=>{
    expect( findWaysForChangeGivenCoins(123,[]) ).to.equal(0);
  })

  it('How many ways needed for 4 cents, given coins [1,2,3]', ()=>{
    expect( findWaysForChangeGivenCoins(4,[1,2,3]) ).to.equal(4);
  })

  it('How many ways needed for 10 cents, given coins [2,5,3,6]', ()=>{
    expect( findWaysForChangeGivenCoins(10,[2,5,3,6]) ).to.equal(5);
  })
})
